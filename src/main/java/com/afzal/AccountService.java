package com.afzal;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/accounts")
public class AccountService {
	List<AccountHolder> accHolders;
	AccountHolder account1, account2, account3;
	
	public AccountService()
	{
		accHolders = new ArrayList<>();
		  account1 = new AccountHolder("John Doe", "123456789", 5000.00, "Savings");
	      account2 = new AccountHolder("Jane Smith", "987654321", 7500.00, "Current");
	      account3 = new AccountHolder("Alice Johnson", "456123789", 10000.00, "Savings");
	      accHolders.add(account1);
	      accHolders.add(account2);
	      accHolders.add(account3);
	      
	}
    @GET
    @Path("/details")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountHolder> getAccountDetails() {
       return accHolders;
    }
}