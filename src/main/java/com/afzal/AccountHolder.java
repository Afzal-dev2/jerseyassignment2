package com.afzal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountHolder {
    @Override
	public String toString() {
		return "AccountHolder [name=" + name + ", accountNumber=" + accountNumber + ", balance=" + balance
				+ ", accountType=" + accountType + "]";
	}

	private String name;
    private String accountNumber;
    private double balance;
    private String accountType;

    public AccountHolder(String name, String accountNumber, double balance, String accountType) {
        this.name = name;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.accountType = accountType;
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}